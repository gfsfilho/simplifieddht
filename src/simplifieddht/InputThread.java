/*
* DCC 064 - Sistemas Distribuídos
* Professor: Ciro Barbosa
* author: Gilmar Ferreira
* author: Hemerson Tacon
* date: 19/10/2016
*/
package simplifieddht;

import framework.Entidade;
import framework.Evento;
import framework.SThread;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InputThread extends SThread {

    public InputThread( Entidade _ent){
        super( _ent);
    }
    @Override
    public void run() {
        String aux1;
        Boolean continua = true;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        while(continua){
            //System.out.print("Informe o comando: ");

            try {
                aux1 = in.readLine();
                String []aux2 = aux1.split(" ");

                switch(aux2[0]){
                    case "remove":
                        //this.ms = "remove";
                        ((Node)ent).colocaEvento(new Evento(5, aux2[0],"empty"));
                        continua = false;
                        break;
                    case "lookup":
                        ((Node)ent).colocaEvento(new Evento(6, aux2[0],
                                aux2[1] +","+ String.valueOf(((Node)ent).getPort())));
                        break;
                    case "stats":
                        ((Node)ent).colocaEvento(new Evento(10, aux2[0],"empty"));
                        break;
                    default:
                        ((Node)ent).colocaEvento(new Evento(0, aux2[0],"<<Comando invalido>>"));
                }
            } catch (IOException ex) {
                Logger.getLogger(Node_idle.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
