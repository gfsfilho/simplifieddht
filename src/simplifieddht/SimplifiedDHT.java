/*
* DCC 064 - Sistemas Distribuídos
* Professor: Ciro Barbosa
* author: Gilmar Ferreira
* author: Hemerson Tacon
* date: 19/10/2016
*/
package simplifieddht;

import framework.Entidade;
import framework.Evento;
import framework.SThread;


public class SimplifiedDHT {
    
    Thread thread1, thread2, thread3;
    SThread sthread;
    SocketThread xthread; 
    InputThread ithread;
    public static final int invalid = 0;
    public static final int insertNew     = 1;
    public static final int askInsertion  = 2;
    public static final int insertionDone = 3;
    public static final int updateNext    = 4;
    public static final int remove        = 5;
    public static final int lookup        = 6;
    public static final int notify        = 7;
    public static final int updatePrev    = 8;
    public static final int answerLookup  = 9;
    Entidade no;

    public static void main(String[] args) {
        // TODO code application logic here
        SimplifiedDHT sDHT = new SimplifiedDHT();
        sDHT.init(args);
    }
    
    public void init(String[] args) {
        switch(args.length) {
            case 2:
                //anel vazio
            case 3:
                //anel ja inicializado
                no = new Node(Integer.parseInt(args[0]), 
                              Integer.parseInt(args[1]));
                
                // Inicia thread de tratamento de eventos
                sthread = new SThread(no);
                thread1 = new Thread(sthread);
                thread1.start();
                
                // Inicia thread de leitura do socket
                xthread = new SocketThread(no.msg, no);
                thread2 = new Thread(xthread);
                thread2.start();
                
                // Inicia thread de leitura de teclado
                ithread = new InputThread(no);
                thread3 = new Thread(ithread);
                thread3.start();
                
                ((Node)no).ms = String.join(",", args);
                ((Node)no).colocaEvento(new Evento(insertNew,"insertNew",((Node)no).ms));
                break;
                
            default:
                System.out.println("Numero de argumentos invalido!!");
                break;
        }
    }
    
}
