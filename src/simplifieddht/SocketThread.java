/*
* DCC 064 - Sistemas Distribuídos
* Professor: Ciro Barbosa
* author: Gilmar Ferreira
* author: Hemerson Tacon
* date: 19/10/2016
*/
package simplifieddht;

import framework.Entidade;
import framework.Evento;
import framework.Msg;
import framework.SimpleThread_r;

public class SocketThread extends SimpleThread_r {
    String n;
    String m;
    int code;
    public SocketThread(Msg _m, Entidade _u){
        super(_m, _u);
    }
    @Override
    public void desempacota(){
                    String [] split;
                    // Desempacota mensagem
                    split = tmp.split(",");
                    code = Integer.valueOf(split[0]);
                    n = split[1];
                    m = split[2];
                    //Trata as mensagens com diferentes números de parâmetros
                    int i = 3;
                    while(i < split.length ){
                        m = m + "," + split[i];
                        ++i;
                    }
                    
                   // Cria Evento
                    Evento e = new Evento(code, n, m);
                   // Coloca no buffer da entidade
                   ent.colocaEvento(e); 
    }
}
