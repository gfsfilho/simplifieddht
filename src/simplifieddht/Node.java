/*
* DCC 064 - Sistemas Distribuídos
* Professor: Ciro Barbosa
* author: Gilmar Ferreira
* author: Hemerson Tacon
* date: 19/10/2016
*/
package simplifieddht;

import framework.Entidade;
import framework.Estado;
import framework.Evento;

/**
 *
 * @author gilmarff
 */
public class Node extends Entidade {
    public String ms;
    public Estado _idle;
    public Estado _starting;
    public Estado _waitingInsertion;
    public Estado _waitingLookup;

    private int port;
    private int id;
    private int nextPort;
    private int nextId;
    private int prevPort;
    private int prevId;

    public Node(int port, int addrNS) {

        super(port);

        _idle             = new Node_idle(this);
        _starting         = new Node_starting(this);
        _waitingInsertion = new Node_waitingInsertion(this);
        _waitingLookup    = new Node_waitingLookup(this);

        this.port = port;
        id        = addrNS;
        nextPort  = -1;
        nextId    = -1;
        prevPort  = -1;
        prevId    = -1;

        setState(_starting);

        System.out.println("Entidade node inicializada usando a porta "+port);
    }

    public int getPort() {

        return this.port;
    }

    public int getId() {

        return this.id;
    }

    public int getPrevPort() {

        return this.prevPort;
    }

    public int getPrevId() {

        return this.prevId;
    }

    public int getNextPort() {

        return this.nextPort;
    }

    public int getNextId() {

        return this.nextId;
    }

    public void setNext(int port, int addrNS) {

        nextPort = port;
        nextId = addrNS;
    }

    public void setPrev(int port, int addrNS) {

        prevPort = port;
        prevId = addrNS;
    }

    public void setState(Estado newState){

        est = newState;

        if(est == _idle){
            System.out.print("\n\nInforme o comando: ");
        }
    }

    public void insert(int port, int addrNS) {

        if(addrNS > id) {
            if((addrNS < nextId) || (nextId <= id)) { // inserção correndo no sentido horário

                // avisa o nó a ser inserido qual será o seu proximo e anterior...
                String m1 = String.valueOf(nextPort)+","+String.valueOf(nextId)+","
                           +String.valueOf(this.port)+","+String.valueOf(id);
                Evento e1 = new Evento(3, "insertionDone", m1);
                msg.conecta("localhost", port);
                msg.envia(e1.toString());
                msg.termina();

                // avisa o próximo nó qual será o seu anterior...
                String m2 = String.valueOf(port)+","+String.valueOf(addrNS);
                Evento e2 = new Evento(8, "updatePrev", m2);
                msg.conecta("localhost", nextPort);
                msg.envia(e2.toString());
                msg.termina();

                nextPort = port;
                nextId = addrNS;
            } else{
                // repassa...
                String m = String.valueOf(port)+","+String.valueOf(addrNS);
                Evento e = new Evento(2, "askInsertion", m);
                msg.conecta("localhost", nextPort);
                msg.envia(e.toString());
                msg.termina();
            }
        } else if((addrNS > prevId) || (prevId >= id)) {

            // inserção correndo no sentido anti-horário

                // avisa o nó a ser inserido qual será o seu proximo e anterior...
                String m1 = String.valueOf(this.port)+","+String.valueOf(id)+","
                           +String.valueOf(this.prevPort)+","+String.valueOf(prevId);
                Evento e1 = new Evento(3, "insertionDone", m1);
                msg.conecta("localhost", port);
                msg.envia(e1.toString());
                msg.termina();

                // avisa o nó anterior qual será o seu próximo...
                String m2 = String.valueOf(port)+","+String.valueOf(addrNS);
                Evento e2 = new Evento(4, "updateNext", m2);
                msg.conecta("localhost", prevPort);
                msg.envia(e2.toString());
                msg.termina();

                prevPort = port;
                prevId = addrNS;

        } else{
            // repassa...
            String m = String.valueOf(port)+","+String.valueOf(addrNS);
            Evento e = new Evento(2, "askInsertion", m);
            msg.conecta("localhost", prevPort);
            msg.envia(e.toString());
            msg.termina();
        }
    }

    public void lookup(int addrNS, int src) {
        /*
        *
        * parâmetros:
        *            addrNS = o endereço em que eu quero fazer o lookup
        *            src = o nó para o qual foi feito o lookup (ele que tem que
        *                  responder)
        *
        *
        * (src == getPort()) = o lookup é um repasse (true) ou não (false)?
        *
        */

        if(id >= addrNS) {

            if((prevId >= id || prevId < addrNS) && (src == getPort())) {
                // o primeiro nó a ser perguntado sabe responder
                System.out.println("Resposta: "+this.port);
                setState(_idle);
            } else if((prevId >= id || prevId < addrNS) && !(src == getPort())){

                // quem sabe é algum nó para o qual o lookup foi repassado
                // neste caso passa a resposta para o nó que está esperando
                String m = "Resposta: " + String.valueOf(this.port);
                Evento e = new Evento(9, "answerLookup", m);
                msg.conecta("localhost", src);
                msg.envia(e.toString());
                msg.termina();
                setState(_idle);
            } else {

                
                // repassa lookup para o anterior
                String m = String.valueOf(addrNS)+","+String.valueOf(src);
                Evento e = new Evento(6, "lookup", m);
                msg.conecta("localhost", prevPort);
                msg.envia(e.toString());
                msg.termina();
                
                // muda o estado para waitingLookup se for o nó origem
                // muda para idle se for um nó que recebeu repasse, para que 
                // ele não fique bloqueado
                if(src == getPort())
                    setState(_waitingLookup);
                else
                    setState(_idle);

            }
        } else if(nextId <= id){ // se está na emenda no anel
            if((src == getPort())){
                // o primeiro nó a ser perguntado sabe responder
                System.out.println("Resposta: "+this.port);
                setState(_idle);
            } else {
                // quem sabe é algum nó para o qual o lookup foi repassado
                // neste caso passa a resposta para o nó que está esperando
                String m = "Resposta: " + String.valueOf(this.port);
                Evento e = new Evento(9, "answerLookup", m);
                msg.conecta("localhost", src);
                msg.envia(e.toString());
                msg.termina();
                setState(_idle);
            }

        } else {

            // repassa lookup para o próximo
            String m = String.valueOf(addrNS)+","+String.valueOf(src);
            Evento e = new Evento(6, "lookup", m);
            msg.conecta("localhost", nextPort);
            msg.envia(e.toString());
            msg.termina();
            
            // muda o estado para waitingLookup
            if(src == getPort())
                setState(_waitingLookup);
            else
                setState(_idle);

        }
    }

    public void remove(){

        // avisa ao nó anterior qual será o seu próximo nó...
        String m1 = String.valueOf(nextPort)+","+String.valueOf(nextId);
        Evento e1 = new Evento(4, "updateNext", m1);
        msg.conecta("localhost", prevPort);
        msg.envia(e1.toString());
        msg.termina();

        // avisa ao próximo nó qual será o seu anterior...
        String m2 = String.valueOf(prevPort)+","+String.valueOf(prevId);
        Evento e2 = new Evento(8, "updatePrev", m2);
        msg.conecta("localhost", nextPort);
        msg.envia(e2.toString());
        msg.termina();

        System.exit(0);
    }

}
