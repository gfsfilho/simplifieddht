/*
* DCC 064 - Sistemas Distribuídos
* Professor: Ciro Barbosa
* author: Gilmar Ferreira
* author: Hemerson Tacon
* date: 19/10/2016
*/
package simplifieddht;

import framework.Entidade;
import framework.Estado;
import framework.Evento;


public class Node_starting extends Estado {
    
    public Node_starting(Entidade _e) {
        super(_e);
    }
    
    @Override
    public void transicao(Evento _ev){
        
        switch(_ev.code){ 
            
            case 1: // Evento insertNew
                
                String[] args = ((Node)ent).ms.split(",");
                
                if(args.length == 2) { // o anel está vazio
                     
                    // simplesmente coloca o proximo nó e o anterior como ele mesmo
                    int port = Integer.parseInt(args[0]);
                    int id   = Integer.parseInt(args[1]);
                    ((Node)ent).setNext(port, id);
                    ((Node)ent).setPrev(port, id);
                    
                    // muda o estado para IDLE
                    ((Node)ent).setState(((Node)ent)._idle);
                    
                } else if(args.length == 3) { // o anel não está vazio
                    
                    // muda estado para WAITINGINSERTION
                    ((Node)ent).setState(((Node)ent)._waitingInsertion);
                    // faz o pedido de inserção
                    int destPort = Integer.parseInt(args[2]);
                    String newMsg = args[0]+","+args[1];
                    Evento e = new Evento(2, "askInsertion", newMsg);
                    ent.msg.conecta("localhost", destPort);
                    ent.msg.envia(e.toString());
                    ent.msg.termina();
                }    
                break;
            default: // evento inesperado
                System.out.println("Node descartou evento : "+_ev.code + " em Starting");
        }
    }
}
