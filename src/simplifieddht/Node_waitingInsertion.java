/*
* DCC 064 - Sistemas Distribuídos
* Professor: Ciro Barbosa
* author: Gilmar Ferreira
* author: Hemerson Tacon
* date: 19/10/2016
*/
package simplifieddht;

import framework.Entidade;
import framework.Estado;
import framework.Evento;


public class Node_waitingInsertion extends Estado {
    
    public Node_waitingInsertion(Entidade _e) {
        super(_e);
    }
    
    @Override
    public void transicao(Evento _ev){
        switch(_ev.code){ 
            case 3: // Evento INSERTIONDONE
                
                String[] args = _ev.msg.split(",");
                int nextPort = Integer.parseInt(args[0]);
                int nextId = Integer.parseInt(args[1]);
                int prevPort = Integer.parseInt(args[2]);
                int prevId = Integer.parseInt(args[3]);
                
                // atualiza o proximo e o anterior
                ((Node)ent).setNext(nextPort, nextId);
                ((Node)ent).setPrev(prevPort, prevId);
                // muda o estado para IDLE  
                ((Node)ent).setState(((Node)ent)._idle);
                break;
                
            default: // evento inesperado
                System.out.println("NODE descartou evento : "+_ev.code + 
                                   " em Waiting Insertion");
        }
    }
}
