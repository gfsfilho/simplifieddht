/*
* DCC 064 - Sistemas Distribuídos
* Professor: Ciro Barbosa
* author: Gilmar Ferreira
* author: Hemerson Tacon
* date: 19/10/2016
*/
package simplifieddht;

import framework.Entidade;
import framework.Estado;
import framework.Evento;


public class Node_waitingLookup extends Estado {
    
    public Node_waitingLookup(Entidade _e) {
        super(_e);
    }
    
    @Override
    public void transicao(Evento _ev){
        switch(_ev.code){ 
            case 9: // Evento ANSWERLOOKUP
                
                // Informa a resposta
                String answer = _ev.msg;
                System.out.println(answer);
                
                // troca estado para idle
                ((Node)ent).setState(((Node)ent)._idle);
                break;                
            default: // evento inesperado
                System.out.println("NODE descartou evento : "+_ev.code + 
                                   " em Waiting Lookup");
        }
    }
}
