/*
* DCC 064 - Sistemas Distribuídos
* Professor: Ciro Barbosa
* author: Gilmar Ferreira
* author: Hemerson Tacon
* date: 19/10/2016
*/
package simplifieddht;

import framework.Entidade;
import framework.Estado;
import framework.Evento;


public class Node_idle extends Estado {

    public Node_idle(Entidade _e) {
        super(_e);
    }
    @Override
    public void transicao(Evento _ev){

        String[] args = _ev.msg.split(",");

        switch(_ev.code){

            case 2: // Evento askInsertion

                int port   = Integer.parseInt(args[0]);
                int addrNS = Integer.parseInt(args[1]);

                // faz a inserção
                ((Node)ent).insert(port, addrNS);

                // Mudança redundante de estado para mostrar a mensagem
                //"Informe o comando:" no console
                ((Node)ent).setState(((Node)ent)._idle);
                break;
             case 4: // Evento updateNext

                int nextPort = Integer.parseInt(args[0]);
                int nextId   = Integer.parseInt(args[1]);

                // atualiza o nó anterior
                ((Node)ent).setNext(nextPort, nextId);
                // Mudança redundante de estado para mostrar a mensagem
                //"Informe o comando:" no console
                ((Node)ent).setState(((Node)ent)._idle);
                break;
            case 5: // Evento remove
                ((Node)ent).remove();
                break;
            case 6: // Evento lookup

                int lkpAddrNS       = Integer.parseInt(args[0]);
                int src             = Integer.parseInt(args[1]);

                // faz o lookup
                ((Node)ent).lookup(lkpAddrNS, src);
                break;
            case 8: // Evento updatePrev

                int prevPort = Integer.parseInt(args[0]);
                int prevId   = Integer.parseInt(args[1]);

                // atualiza o nó anterior
                ((Node)ent).setPrev(prevPort, prevId);
                // Mudança redundante de estado para mostrar a mensagem
                //"Informe o comando:" no console
                ((Node)ent).setState(((Node)ent)._idle);
                break;
            case 10: // evento de debug
                System.err.println("\n------- Prev ---- This ---- Next ----");
                System.err.println(  " Port   "+((Node)ent).getPrevPort()+" ---- "+((Node)ent).getPort()+" ---- "+((Node)ent).getNextPort()+" ----");
                System.err.println(  "   Id     "+((Node)ent).getPrevId()+" ------ "+((Node)ent).getId()+" ------ "+((Node)ent).getNextId()+" -----");
                ((Node)ent).setState(((Node)ent)._idle);
                break;
            default: // evento inesperado
                System.out.println("NODE descartou evento : "+_ev.code + " em Idle");
                ((Node)ent).setState(((Node)ent)._idle);
        }
    }
}
